/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pubsub

import (
	"context"
	"sync"
	"testing"
)

func TestHub(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	p := newHub()
	events, errc := p.Subscribe(ctx)

	got, err := p.Subscribers()
	if err != nil {
		t.Errorf("Test failed with an error: %s", err.Error())
		return
	}
	t.Log(got)

	w := sync.WaitGroup{}
	w.Add(1)

	go func() {
		p.Publish(ctx, new(Message))
		p.Publish(ctx, new(Message))
		p.Publish(ctx, new(Message))
		w.Done()
	}()
	w.Wait()

	w.Add(3)
	go func() {
		defer w.Done()
		for {
			select {
			case <-errc:
				return
			case v := <-events:
				println(v)
				return
			}
		}
	}()
	w.Wait()

	cancel()
}
