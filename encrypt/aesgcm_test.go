/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package encrypt

import "testing"

func TestAesgcm(t *testing.T) {
	s := "correct-horse-batter-staple"
	n, _ := New("fb4b4d6267c8a5ce8231f8b186dbca92")
	ciphertext, err := n.Encrypt(s)
	if err != nil {
		t.Error(err)
	}
	plaintext, err := n.Decrypt(ciphertext)
	if err != nil {
		t.Error(err)
	}
	if want, got := plaintext, s; got != want {
		t.Errorf("Want plaintext %q, got %q", want, got)
	}
}

func TestAesgcmFail(t *testing.T) {
	s := "correct-horse-batter-staple"
	n, _ := New("ea1c5a9145c8a5ce8231f8b186dbcabc")
	ciphertext, err := n.Encrypt(s)
	if err != nil {
		t.Error(err)
	}
	n, _ = New("fb4b4d6267c8a5ce8231f8b186dbca92")
	_, err = n.Decrypt(ciphertext)
	if err == nil {
		t.Error("Expect error when encryption and decryption keys mismatch")
	}
}

func TestAesgcmCompat(t *testing.T) {
	s := "correct-horse-batter-staple"
	n, _ := New("")
	ciphertext, err := n.Encrypt(s)
	if err != nil {
		t.Error(err)
	}
	n, _ = New("ea1c5a9145c8a5ce8231f8b186dbcabc")
	n.(*Aesgcm).Compat = true
	plaintext, err := n.Decrypt(ciphertext)
	if err != nil {
		t.Error(err)
	}
	if want, got := plaintext, s; got != want {
		t.Errorf("Want plaintext %q, got %q", want, got)
	}
}
