/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mysql

//go:generate togo ddl -package mysql -dialect mysql


// input files/*.sql
/*
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "package",
			Value: "ddl",
		},
		cli.StringFlag{
			Name:  "dialect",
			Value: "sqlite3",
		},
		cli.StringFlag{
			Name:  "input",
			Value: "files/*.sql",
		},
		cli.StringFlag{
			Name:  "output",
			Value: "ddl_gen.go",
		},
		cli.BoolFlag{
			Name: "log",
		},
		cli.StringFlag{
			Name:  "logger",
			Value: "log", // log, logrus
		},
 */